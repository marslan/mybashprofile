#-----------------------------------------------------------------------
# Set Java JDK Version
#-----------------------------------------------------------------------
function setjdk() {
  if [ $# -ne 0 ]; then
   removeFromPath '/System/Library/Frameworks/JavaVM.framework/Home/bin'
   if [ -n "${JAVA_HOME+x}" ]; then
    removeFromPath $JAVA_HOME
   fi
   export JAVA_HOME=`/usr/libexec/java_home -v $@`
   export PATH=$JAVA_HOME/bin:$PATH
   echo 'Set Java Version: ' $@
   fi
 }
# Remove from Path.
function removeFromPath() {
  export PATH=$(echo $PATH | sed -E -e "s;:$1;;" -e "s;$1:?;;")
}

#------------------------------------
# Mongo DB
#------------------------------------
alias mongostart="launchctl start org.mongodb.mongod"
alias mongostop="launchctl stop org.mongodb.mongod"
alias mongorestart="launchctl restart org.mongodb.mongod"

#--------------------------------
# Vagrant Default Provider
#--------------------------------
export VAGRANT_DEFAULT_PROVIDER=virtualbox

#------------------------------------
# Node Webkit Alias
#------------------------------------
alias nw=nodewebkit

#------------------------------------
# Docker path variables
#------------------------------------
export DOCKER_HOST=tcp://192.168.59.103:2376
export DOCKER_CERT_PATH=/Users/marslan/.boot2docker/certs/boot2docker-vm
export DOCKER_TLS_VERIFY=1

#----------------------------------------
# Show/Hide files alias
#----------------------------------------
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'
alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'

#-------------------------------------------------------
# Function to start a google search from command-line
#-------------------------------------------------------
function google() { open /Applications/Safari.app/ "http://www.google.com/search?q= $1"; }

#----------------------------------------------
# lets clear the screen with just 1 Letter
#----------------------------------------------
alias c='clear'

#------------------------------
# change folder quicker
#------------------------------
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'

#----------------------------
# Some Listing Shortcuts
#----------------------------
alias l='ls -l'
alias ld='ls -ld'
alias ll='ls -ltra'

#------------------------------
# Home directory
#------------------------------
alias home='cd ~/'

#---------------------------
# SEARCHING
#---------------------------
alias qfind="find . -name "  # qfind:    Quickly search for file

	ff () { /usr/bin/find . -name "$@" ; }      # ff:       Find file under the current directory

	ffs () 	{ 
		/usr/bin/find . -name "$@"'*' ; 
		}  # ffs:      Find file whose name starts with a given string

	ffe ()  { 
		/usr/bin/find . -name '*'"$@" ;
		 }  # ffe:      Find file whose name ends with a given string

#---------------------------
# NETWORKING
#---------------------------

alias myip='curl ip.appspot.com'                    # myip:         Public facing IP Address
alias netCons='lsof -i'                             # netCons:      Show all open TCP/IP sockets
alias flushDNS='dscacheutil -flushcache'            # flushDNS:     Flush out the DNS Cache
alias lsock='sudo /usr/sbin/lsof -i -P'             # lsock:        Display open sockets
alias lsockU='sudo /usr/sbin/lsof -nP | grep UDP'   # lsockU:       Display only open UDP sockets
alias lsockT='sudo /usr/sbin/lsof -nP | grep TCP'   # lsockT:       Display only open TCP sockets
alias ipInfo0='ipconfig getpacket en0'              # ipInfo0:      Get info on connections for en0
alias ipInfo1='ipconfig getpacket en1'              # ipInfo1:      Get info on connections for en1
alias openPorts='sudo lsof -i | grep LISTEN'        # openPorts:    All listening connections
alias showBlocked='sudo ipfw list'                  # showBlocked:  All ipfw rules inc/ blocked IPs


#------------------------------------------------------------------
#   ii:  display useful host related informaton
#-------------------------------------------------------------------
    ii() {
        echo -e "\nYou are logged on ${RED}$HOST"
        echo -e "\nAdditionnal information:$NC " ; uname -a
        echo -e "\n${RED}Users logged on:$NC " ; w -h
        echo -e "\n${RED}Current date :$NC " ; date
        echo -e "\n${RED}Machine stats :$NC " ; uptime
        echo -e "\n${RED}Current network location :$NC " ; scselect
        echo -e "\n${RED}Public facing IP Address :$NC " ; myip
        #echo -e "\n${RED}DNS Configuration:$NC " ; scutil --dns
        echo
	    }

#-----------------------------------------------------------------------------------
#   screensaverDesktop: Run a screensaver on the Desktop
#-----------------------------------------------------------------------------------
    alias screensaverDesktop='/System/Library/Frameworks/ScreenSaver.framework/Resources/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine -background'


#---------------------------------------
#   WEB DEVELOPMENT
#---------------------------------------

alias apacheEdit='sudo edit /etc/httpd/httpd.conf'      # apacheEdit:       Edit httpd.conf
alias apacheRestart='sudo apachectl graceful'           # apacheRestart:    Restart Apache
alias editHosts='sudo edit /etc/hosts'                  # editHosts:        Edit /etc/hosts file
alias herr='tail /var/log/httpd/error_log'              # herr:             Tails HTTP error logs
alias apacheLogs="less +F /var/log/apache2/error_log"   # Apachelogs:   Shows apache error logs
httpHeaders () { /usr/bin/curl -I -L $@ ; }             # httpHeaders:      Grabs headers from web page

#---------------------------------------------------------------------
#   httpDebug:  Download a web page and show info on what took time
#---------------------------------------------------------------------
    httpDebug () { /usr/bin/curl $@ -o /dev/null -w "dns: %{time_namelookup} connect: %{time_connect} pretransfer: %{time_pretransfer} starttransfer: %{time_starttransfer} total: %{time_total}\n" ; }

#----------------------------------------
# GIT
#----------------------------------------
git config --global user.name "Muhammad Arslan"
git config --global user.email "arslan_mecom@yahoo.com"
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto
git config --global core.autocrlf input
git config --global push.default simple

export GIT_SSL_NO_VERIFY=true # Enables git to work with self signed SSL certs

#--------------------------------
# Local IP
#--------------------------------
alias myLocalIp='ifconfig | grep inet'

#---------------------------------------------------------------------
# Back Up done
#---------------------------------------------------------------------
  backupDone() {
	$HOME/scripts/pushover.sh;
}
